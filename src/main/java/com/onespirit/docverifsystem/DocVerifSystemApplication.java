package com.onespirit.docverifsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocVerifSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocVerifSystemApplication.class, args);
	}

}
