package com.onespirit.docverifsystem.controllers;

import com.nimbusds.jose.JWEDecrypter;
import com.nimbusds.jose.JWEEncrypter;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.onespirit.docverifsystem.exception.DataNotFoundException;
import com.onespirit.docverifsystem.model.Diploma;
import com.onespirit.docverifsystem.model.DiplomaDetails;
import com.onespirit.docverifsystem.model.Speciality;
import com.onespirit.docverifsystem.model.Student;
import com.onespirit.docverifsystem.repository.DiplomaRepository;
import com.onespirit.docverifsystem.repository.SpecialtyRepository;
import com.onespirit.docverifsystem.repository.StudentRepository;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.TextCodec;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.security.Key;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Logger;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class DiplomaController {

    @Autowired
    DiplomaRepository diplomaRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    SpecialtyRepository specialtyRepository;

    private static final Logger logger = Logger.getLogger(DiplomaController.class.getName());
    private static final String SECRET_KEY = "DocVerifSystem";

    @GetMapping("/diploma")
    public Iterable<Diploma> getAllDiplomas() {
        return diplomaRepository.findAll();
    }

    @GetMapping("/diploma/reference/{reference}")
    public Iterable<Diploma> getDiplomaByReference(@PathVariable String reference) {
        JWEDecrypter decrypter = new RSADecrypter(rsaPrivateKey);
        Iterable<Diploma> diploma = diplomaRepository.findByReference(reference);
        Diploma d = diploma.iterator().next();
        this.decodeJWT(d.getDiplomaJwt(), d.getReference());
        logger.info("Diploma" +  d);
        logger.info("Diploma" +  this.decodeJWT(d.getDiplomaJwt(), d.getReference()));
        return diploma;
    }

    @PostMapping("/diploma")
    public Diploma createDiploma(@RequestBody DiplomaDetails diplomaDetails) throws DataNotFoundException {
        JWEEncrypter encrypter = new RSAEncrypter(rsaPublicKey);
        String studentJwt = this.generateStudentJWT(diplomaDetails.getMatricule(), diplomaDetails.getFirstName(),
                diplomaDetails.getLastName(), diplomaDetails.getLevel(), diplomaDetails.getSpeciality());
        Student student = new Student(studentJwt);
//        this.studentRepository.save(student);
        String reference = this.getReference();
        Diploma diploma = null;
        Date date = new Date();
        String diplomaJwt = this.generateDiplomaJWT(reference, date, diplomaDetails.getAcademicYear(), diplomaDetails.getAverage(), student);
        diploma = new Diploma(reference, diplomaJwt, student);

        return this.diplomaRepository.save(diploma);
    }

//    private String generateJwt(String reference, Date date, String academicYear, double average, Student student) throws UnsupportedEncodingException {
//        String jws = Jwts.builder()
//                .claim("reference", reference)
//                .claim("date", date.toString())
//                .claim("academicYear", academicYear)
//                .claim("average", average)
//                .claim("student", student.toString())
//                .signWith(
//                        SignatureAlgorithm.HS256,
//                        reference + "DocVerifSystem".getBytes("UTF-8")
//                )
//                .compact();
//        System.out.println(jws);
//        return jws;
//    }

    public String generateDiplomaJWT(String reference, Date date, String academicYear, double average, Student student) {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(reference + SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder()
                .claim("reference", reference)
                .claim("date", date.toString())
                .claim("academicYear", academicYear)
                .claim("average", average)
                .claim("student", student.toString())
                .signWith(signatureAlgorithm, signingKey);

        //if it has been specified, let's add the expiration
//        if (ttlMillis > 0) {
//            long expMillis = nowMillis + ttlMillis;
//            Date exp = new Date(expMillis);
//            builder.setExpiration(exp);
//        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    public String generateStudentJWT(String matricule, String firstName, String lastName, String level, String speciality) {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(matricule + SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder()
                .claim("matricule", matricule)
                .claim("firstName", firstName)
                .claim("lastName", lastName)
                .claim("level", level)
                .claim("speciality", speciality)
                .signWith(signatureAlgorithm, signingKey);

        //if it has been specified, let's add the expiration
//        if (ttlMillis > 0) {
//            long expMillis = nowMillis + ttlMillis;
//            Date exp = new Date(expMillis);
//            builder.setExpiration(exp);
//        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    public String getReference() {
        return "CMR-" + Integer.valueOf((int) ((Math.random()*((400)+1))+100));
    }

    public static Claims decodeJWT(String jwt, String reference) {
        //This line will throw an exception if it is not a signed JWS (as expected)
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(reference + SECRET_KEY))
                .parseClaimsJws(jwt).getBody();
        return claims;
    }

    @PostMapping("/diplomaList")
    public ResponseEntity<Iterable<Diploma>> uploadData(@RequestParam("file") MultipartFile file) throws Exception {
        if (file == null) {
            throw new RuntimeException("You must select the a file for uploading");
        }

        Iterable<Diploma> itDiploma = null;
        String originalName = file.getOriginalFilename();

        try {
            InputStream inputStream = file.getInputStream();
            String name = file.getName();
            String contentType = file.getContentType();
            long size = file.getSize();

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();

            ArrayList<Diploma> diplomaList = new ArrayList<Diploma>();
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
                if (row == sheet.getRow(0)){
//                    logger.info("log cell ::" + sheet.getRow(0).getCell(0));
                    continue;
                }
                logger.info(row.getCell(0) + ", " + row.getCell(1) +
                        ", " + row.getCell(2) + ", " + row.getCell(3) + ", " + row.getCell(4) +
                        ", " + row.getCell(5) + ", " + row.getCell(6));
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                String matricule = String.valueOf(row.getCell(0));
                String lastName = String.valueOf(row.getCell(1));
                String firstName = String.valueOf(row.getCell(2));
                String academicYear = String.valueOf(row.getCell(3));
                String speciality = String.valueOf(row.getCell(4));
                String level = String.valueOf(row.getCell(5));
                double average = Double.valueOf(String.valueOf(row.getCell(6)));

                String studentJwt = this.generateStudentJWT(matricule, firstName, lastName, level, speciality);
                Student student = new Student(studentJwt);
//                String reference = "REF-" + + (Math.random()*((4000)+1))+1000;
                String reference = this.getReference();
                Date issueDate = new Date();
                String diplomaJwt = this.generateDiplomaJWT(reference, issueDate, academicYear, average, student);
                Diploma diploma = new Diploma(reference, diplomaJwt, student);
                diplomaList.add(diploma);
//                while (cellIterator.hasNext())
//                {
//                    Cell cell = cellIterator.next();
//                    //Check the cell type and format accordingly
//                    logger.info("log cell data" + cell.toString());
//                }
            }
            itDiploma = diplomaList;
            diplomaRepository.saveAll(itDiploma);
        } catch(Exception ioe) {
            ioe.printStackTrace();
        }

        // Do processing with uploaded file data in Service layer
        return new ResponseEntity<Iterable<Diploma>>(itDiploma, HttpStatus.OK);
    }

    public void uploadFile(Model model, MultipartFile file) throws IOException {
        try {
//            File tempFile = new File("Temp File");
//            file.transferTo(tempFile);
            InputStream inputStream =  file.getInputStream();
            POIFSFileSystem fs = new POIFSFileSystem(inputStream);
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            HSSFRow row;
            HSSFCell cell;

            int rows; // No of rows
            rows = sheet.getPhysicalNumberOfRows();

            int cols = 0; // No of columns
            int tmp = 0;

            // This trick ensures that we get the data properly even if it doesn't start from first few rows
            for(int i = 0; i < 10 || i < rows; i++) {
                row = sheet.getRow(i);
                if(row != null) {
                    tmp = sheet.getRow(i).getPhysicalNumberOfCells();
                    if(tmp > cols) cols = tmp;
                }
            }

            for(int r = 0; r < rows; r++) {
                row = sheet.getRow(r);
                if(row != null) {
                    for(int c = 0; c < cols; c++) {
                        cell = row.getCell((short)c);
                        if(cell != null) {
                            System.out.println(cell);
                        }
                    }
                }
            }
        } catch(Exception ioe) {
            ioe.printStackTrace();
        }
    }

}
