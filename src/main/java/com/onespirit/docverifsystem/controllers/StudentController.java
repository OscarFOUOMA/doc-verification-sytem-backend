package com.onespirit.docverifsystem.controllers;

import com.onespirit.docverifsystem.model.Student;
import com.onespirit.docverifsystem.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class StudentController {

    @Autowired
    StudentRepository studentRepository;

    @GetMapping("/student")
    public Iterable<Student> getAllStudent() {
        return studentRepository.findAll();
    }
}
