package com.onespirit.docverifsystem.controllers;

import com.onespirit.docverifsystem.exception.DataNotFoundException;
import com.onespirit.docverifsystem.model.Speciality;
import com.onespirit.docverifsystem.repository.SpecialtyRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class SpecialityController {

    @Autowired
    SpecialtyRepository specialtyRepository;

    //	get all universities
    @GetMapping("/speciality")
    @ApiOperation(value = "View a list of universities")
    public Iterable<?> getAllUniversities() {
        return specialtyRepository.findAll();
    }

    //	create speciality
    @PostMapping("/speciality")
    public Speciality createSpeciality(@Valid @RequestBody Speciality speciality) {
        return specialtyRepository.save(speciality);
    }

    //	get a single speciality
    @GetMapping("/speciality/{id}")
    public Speciality getSpecialityById(@PathVariable int id) throws DataNotFoundException {
        return specialtyRepository.findById(id)
                .orElseThrow(()->new DataNotFoundException());
    }
}
