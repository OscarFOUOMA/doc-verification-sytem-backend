package com.onespirit.docverifsystem.controllers;

import com.onespirit.docverifsystem.exception.DataNotFoundException;
import com.onespirit.docverifsystem.model.Faculty;
import com.onespirit.docverifsystem.repository.FacultyRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class FacultyController {

    @Autowired
    FacultyRepository facultyRepository;

    //	get all faculties
    @GetMapping("/faculty")
    @ApiOperation(value = "View a list of faculties")
    public Iterable<?> getAllFaculties() {
        return facultyRepository.findAll();
    }

    //	create faculty
    @PostMapping("/faculty")
    public Faculty createFaculty(@Valid @RequestBody Faculty faculty) {
        return facultyRepository.save(faculty);
    }

    //	get a single faculty
    @GetMapping("/faculty/{id}")
    public Faculty getFacultyById(@PathVariable int id) throws DataNotFoundException {
        return facultyRepository.findById(id)
                .orElseThrow(()->new DataNotFoundException());
    }
}
