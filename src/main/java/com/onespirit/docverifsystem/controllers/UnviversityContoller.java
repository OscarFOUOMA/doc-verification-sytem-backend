package com.onespirit.docverifsystem.controllers;

import com.onespirit.docverifsystem.exception.DataNotFoundException;
import com.onespirit.docverifsystem.model.University;
import com.onespirit.docverifsystem.repository.UniversityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class UnviversityContoller {

    @Autowired
    UniversityRepository universityRepository;

    @GetMapping("/university")
    public Iterable<University> getAllUniversities() {
        return universityRepository.findAll();
    }

    //	create university
    @PostMapping("/university")
    public University createUniversity(@Valid @RequestBody University university) {
        return universityRepository.save(university);
    }

    //	get a single university
    @GetMapping("/university/{id}")
    public University getUniversityById(@PathVariable int id) throws DataNotFoundException {
        return universityRepository.findById(id)
                .orElseThrow(()->new DataNotFoundException());
    }
}
