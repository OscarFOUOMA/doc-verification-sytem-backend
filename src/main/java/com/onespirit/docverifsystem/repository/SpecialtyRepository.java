package com.onespirit.docverifsystem.repository;

import com.onespirit.docverifsystem.model.Speciality;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecialtyRepository extends JpaRepository<Speciality, Integer> {
    Speciality findByTitle(String title);
}
