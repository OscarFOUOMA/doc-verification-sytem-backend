package com.onespirit.docverifsystem.repository;

import com.onespirit.docverifsystem.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Integer> {
}
