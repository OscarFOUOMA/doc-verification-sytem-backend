package com.onespirit.docverifsystem.repository;

import com.onespirit.docverifsystem.model.University;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UniversityRepository extends JpaRepository<University, Integer> {
}
