package com.onespirit.docverifsystem.repository;

import com.onespirit.docverifsystem.model.Faculty;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FacultyRepository extends JpaRepository<Faculty, Integer> {
}
