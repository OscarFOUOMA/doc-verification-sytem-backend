package com.onespirit.docverifsystem.repository;

import com.onespirit.docverifsystem.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository  extends JpaRepository<Product, Integer> {
    @Override
    void delete(Product deleted);
}
