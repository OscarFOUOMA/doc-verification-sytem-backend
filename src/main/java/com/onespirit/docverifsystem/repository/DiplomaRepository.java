package com.onespirit.docverifsystem.repository;

import com.onespirit.docverifsystem.model.Diploma;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DiplomaRepository extends JpaRepository<Diploma, Integer> {
    Iterable<Diploma> findByReference(String reference);
}
