package com.onespirit.docverifsystem.repository;

import com.onespirit.docverifsystem.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByEmail(String email);
}
