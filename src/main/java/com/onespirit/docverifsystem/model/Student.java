package com.onespirit.docverifsystem.model;
// Generated 3 nov. 2019 02:27:40 by Hibernate Tools 4.3.5.Final

import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table
public class Student implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private int id;
//
//	@Transient
//	private String matricule;
//
//	@Transient
//	private String firstName;
//
//	@Transient
//	private String lastName;
//
//	@Transient
//	private String level;
//
//	@Transient
//	private String speciality;

	@Column
	@Type(type="text")
	private String studentJwt;

	public Student() {
	}

	public Student(String studentJwt) {
		this.studentJwt = studentJwt;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

//	public String getMatricule() {
//		return matricule;
//	}
//
//	public void setMatricule(String matricule) {
//		this.matricule = matricule;
//	}
//
//	public String getFirstName() {
//		return firstName;
//	}
//
//	public void setFirstName(String firstName) {
//		this.firstName = firstName;
//	}
//
//	public String getLastName() {
//		return lastName;
//	}
//
//	public void setLastName(String lastName) {
//		this.lastName = lastName;
//	}
//
//	public String getLevel() {
//		return level;
//	}
//
//	public void setLevel(String level) {
//		this.level = level;
//	}
//
//	public String getSpeciality() {
//		return speciality;
//	}
//
//	public void setSpeciality(String speciality) {
//		this.speciality = speciality;
//	}

	public void setDiplomaJwt(String studentJwt) {
		this.studentJwt = studentJwt;
	}

	private String getApreciationByAverage(double average) {
		if (average >=10 && average < 12)
			return "Passable";
		else if (average >= 12 && average< 14)
			return "Assez bien";
		else if (average >= 15 && average < 17)
			return "Bien";
		else if (average >= 17 && average<18)
			return "Tres Bien";
		else if (average >= 17 && average <=20)
			return "Excelent";
		return "Aucune Mention";
	}
}
