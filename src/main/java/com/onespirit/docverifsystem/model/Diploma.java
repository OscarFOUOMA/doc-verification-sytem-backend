package com.onespirit.docverifsystem.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

// Generated 3 nov. 2019 02:27:40 by Hibernate Tools 4.3.5.Final

@Entity
@Table
public class Diploma implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private int id;

	@Column
	private String reference;
//
//	@Transient
//	private Date issueDate;
//
//	@Transient
//	private String academicYear;
//
//	@Transient
//	private double average;
//
//	@Transient
//	private String apreciation;

	@Column
	@Type(type="text")
	private String diplomaJwt;

	@OneToOne(cascade=CascadeType.ALL)
	private Student student;

	public Diploma() {
	}

	public Diploma(String reference, String diplomaJwt, Student student) {
		this.reference = reference;
		this.diplomaJwt = diplomaJwt;
		this.student = student;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

//	public Date getIssueDate() {
//		return issueDate;
//	}
//
//	public void setIssueDate(Date issueDate) {
//		this.issueDate = issueDate;
//	}
//
//	public String getAcademicYear() {
//		return academicYear;
//	}
//
//	public void setAcademicYear(String academicYear) {
//		this.academicYear = academicYear;
//	}
//
//	public double getAverage() {
//		return average;
//	}
//
//	public void setAverage(double average) {
//		this.average = average;
//	}
//
//	public String getApreciation() {
//		return apreciation;
//	}
//
//	public void setApreciation(String apreciation) {
//		this.apreciation = apreciation;
//	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public String getDiplomaJwt() {
		return diplomaJwt;
	}

	public void setDiplomaJwt(String diplomaJwt) {
		this.diplomaJwt = diplomaJwt;
	}

	private String getApreciationByAverage(double average) {
		if (average >=10 && average < 12)
			return "Passable";
		else if (average >= 12 && average< 14)
			return "Assez bien";
		else if (average >= 15 && average < 17)
			return "Bien";
		else if (average >= 17 && average<18)
			return "Tres Bien";
		else if (average >= 17 && average <=20)
			return "Excelent";
		return "Aucune Mention";
	}
}
