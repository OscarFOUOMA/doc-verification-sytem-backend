package com.onespirit.docverifsystem.model;

public class DiplomaDetails {
    private String matricule;
    private String firstName;
    private String lastName;
    private String academicYear;
    private String speciality;
    private String level;
    private double average;

    public DiplomaDetails() {
    }

    public DiplomaDetails(String matricule, String firstName, String lastName, String academicYear, String faculty, String speciality, String level, double average) {
        this.matricule = matricule;
        this.firstName = firstName;
        this.lastName = lastName;
        this.academicYear = academicYear;
        this.speciality = speciality;
        this.level = level;
        this.average = average;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }
}
