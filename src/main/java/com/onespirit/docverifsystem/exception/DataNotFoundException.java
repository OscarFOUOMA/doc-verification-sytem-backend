package com.onespirit.docverifsystem.exception;

public class DataNotFoundException extends Exception {

    public DataNotFoundException() {
        super(String.format("Data nor found"));
    }


}
